import './ArtNews.scss'

import React, { Component } from 'react'
import SwiperCore, { Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

import TitleStd from '../TitleStd/TitleStd'

SwiperCore.use([Navigation]);

const artNewsSliderConfig = {
    direction: 'horizontal',
    slidesPerView: 1,
    loop: true,
    breakpoints: {
        959: {
            slidesPerView: 2,
        }
    },
    navigation: {
        nextEl: '.art-news .js-next',
        prevEl: '.art-news .js-prev',
    },
}

class ArtNews extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const artNewsSlides = this.props.data.slides
        return (
            <section className="art-news">
                <TitleStd text='Новости искусства' modClass='art-news__title' />
                <div className="container relative">
                    <div className="art-news__slider-controls">
                        <div className="art-news__prev js-prev slider-control-btn slider-control-btn--prev"></div>
                        <div className="art-news__next js-next slider-control-btn "></div>
                    </div>
                    <Swiper {...artNewsSliderConfig} className="art-news__inner js-news-slider swiper-container">
                        {artNewsSlides.map(({ imgPath, category, date, title, desc }, ind) => {
                            return <SwiperSlide key={ind} className="swiper-slide art-news-item">
                                <div className="art-news-item__img" style={{ backgroundImage: `url('${imgPath}')` }}></div>
                                <div className="art-news-item__content js-eq-height"><a className="art-news-item__rubric" href="#">{category}</a>
                                    <time className="art-news-item__date">{date}</time>
                                    <div className="art-news-item__text-content">
                                        <h3 className="art-news-item__title">{title}</h3>
                                        <p className="art-news-item__text">{desc}</p>
                                    </div><a className="art-news-item__btn btn-arrow" href="#">Смотреть и читать</a>
                                </div>
                            </SwiperSlide>
                        })}
                    </Swiper >
                </div >
            </section >
        )
    }
}
export default ArtNews
