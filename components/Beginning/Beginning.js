import './Beginning.scss'

import { useState, useEffect, useContext, useRef } from 'react'
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock'
import { MainContext } from '../../MainContext'

import TitleStd from '../TitleStd/TitleStd'

const videoImg = 'img/youtube.jpg'
const videoTitle = 'Видеообзор нашего ресурса - возможности для пользователя'
const videoURL = 'https://www.youtube.com/embed/dQw4w9WgXcQ?enablejsapi=1&amp;'

const expertImg = 'img/expert.jpg'
const expertRubricTitle = 'Выбор экспертов'
const expertTitle = 'Обзор нашего эксперта Элеоноры Рубинштейн-Загорской'


function Beginning() {

    const { toggleHeaderShow } = useContext(MainContext)
    const videoElem = useRef(null);
    const [videoOpened, setVideoOpened] = useState(false)

    useEffect(() => {
        !videoOpened ? enableBodyScroll(videoElem) : disableBodyScroll(videoElem);
        return () => { clearAllBodyScrollLocks() }
    })

    function toggleVideo() {
        toggleHeaderShow()
        setVideoOpened(prev => !prev)
    }

    return (
        <section className="beginning">
            <TitleStd text='Не знаете, с чего начать?' modClass='beginning__title-std' />
            <div className="container">
                <div className="beginning__content">
                    <div className="beginning__video video-item js-open-video" style={{ backgroundImage: `url('${videoImg}')` }} onClick={toggleVideo}>
                        <svg className="video-item__icon">
                            <use xlinkHref="#video"></use>
                        </svg>
                        <div className="video-item__title">{videoTitle}</div>
                    </div>
                    {videoOpened && <div className="video-wrapper" ref={videoElem}>
                        <div className="video-close js-close-video" onClick={toggleVideo}></div>
                        <div className="video js-video-screen">
                            <div className="video__frame">
                                <iframe src={videoURL} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen="allowFullScreen"></iframe>
                            </div>
                        </div>
                    </div>}
                    <div className="beginning__expert-choice expert-choice"><a className="expert-choice__img" href="#" style={{ backgroundImage: `url('${expertImg}')` }}></a>
                        <div className="expert-choice__rubric title-small"><a className="title-small__content">{expertRubricTitle}</a></div><a className="expert-choice__link">{expertTitle}</a>
                    </div>
                </div>
            </div >
        </section >
    )
}

export default Beginning
