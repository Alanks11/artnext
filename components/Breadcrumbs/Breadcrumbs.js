import './Breadcrumbs.scss'
import React from 'react';
import Link from 'next/link';

function Breadcrumbs(props) {
    return (
        <div className="item-breadcrumbs">
            <div className="container">
                <div className="item-breadcrumbs__inner">
                    <Link href="/catalog"><a className="item-breadcrumbs__back" >Назад</a></Link>
                    <Link href="/"><a className="item-breadcrumbs__link">Главная</a></Link>
                    <Link href="/catalog"><a className="item-breadcrumbs__link">Каталог</a></Link>
                    <span className="item-breadcrumbs__current">Распоряжения о структуре длинное название картины</span>
                </div>
            </div>
        </div>
    )
}

export default Breadcrumbs
