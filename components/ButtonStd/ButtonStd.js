import './ButtonStd.scss'

import React from 'react'

function ButtonStd(props) {
    return (
        <a className={`btn-std ${props.modClass}`}>{props.text}</a>
    )
}
export default ButtonStd
