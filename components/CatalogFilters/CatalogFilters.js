import './CatalogFilters.scss'

import React, { useState } from 'react'
import cn from 'classnames'
import { Range } from 'rc-slider'
import 'rc-slider/assets/index.css'
import Select from 'react-select'
import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css'



const selectStyles = {
    control: (styles, { isFocused }) => ({
        ...styles,
        width: '30vw',
        border: isFocused ? 'none' : '1px solid gray',
        boxShadow: isFocused ? '1px 1px 3px gray' : 'none',
        margin: '0 auto',
        "@media only screen and (max-width: 768px)": {
            width: '90vw',
        },
    }),
    indicatorSeparator: styles => ({ ...styles, display: 'none' }),
    groupHeading: styles => ({ ...styles, fontSize: '18px', color: 'black', fontWeight: '600' }),
}

function CatalogFilters({ authorOptions, jenreOptions, colorOptions }) {

    //AUTHORS
    function MenuListAuthors(props) {
        return (
            <div style={{ height: '200px' }}>
                <PerfectScrollbar>
                    {props.children}
                </PerfectScrollbar>
            </div>
        )
    }
    function OptionAuthors(props) {
        return (
            <div className={cn('filter-option',
                { 'selected-option': selectedAuthors.find(item => item.value === props.value) ? true : false })}
                data-value={props.value}
                data-label={props.label}
                onClick={addAuthor}>
                {props.children}
            </div>
        )
    }
    const componentsAuthors = {
        MenuList: MenuListAuthors,
        Option: OptionAuthors
    }
    const [selectedAuthors, setSelectedAuthors] = useState([])

    function addAuthor(e) {
        let unique = true
        for (let i = 0; i < selectedAuthors.length; i++) {
            if (selectedAuthors[i].value === e.currentTarget.dataset.value) {
                unique = false;
                break;
            }
        }
        if (unique) {
            setSelectedAuthors([...selectedAuthors, { label: e.currentTarget.dataset.label, value: e.currentTarget.dataset.value }])
            console.log(e.currentTarget);
            e.currentTarget.classList.add('selected-option')
        }
    }
    function removeAuthor(e) {
        setSelectedAuthors(selectedAuthors.filter((item) => item.value !== e.target.dataset.value))
    }

    //JENRES
    function MenuListJenres(props) {
        return (
            <div style={{ height: '200px' }}>
                <PerfectScrollbar>
                    {props.children}
                </PerfectScrollbar>
            </div>
        )
    }
    function OptionJenres(props) {
        return (
            <div className={cn('filter-option',
                { 'selected-option': selectedJenres.find(item => item.value === props.value) ? true : false })}
                data-value={props.value}
                data-label={props.label}
                onClick={addJenre}>
                {props.children}
            </div>
        )
    }
    const componentsJenres = {
        MenuList: MenuListJenres,
        Option: OptionJenres
    }
    const [selectedJenres, setSelectedJenres] = useState([])

    function addJenre(e) {
        let unique = true
        for (let i = 0; i < selectedJenres.length; i++) {
            if (selectedJenres[i].value === e.currentTarget.dataset.value) {
                unique = false;
                break;
            }
        }
        if (unique) {
            setSelectedJenres([...selectedJenres, { label: e.currentTarget.dataset.label, value: e.currentTarget.dataset.value }])
            console.log(e.currentTarget);
            e.currentTarget.classList.add('selected-option')
        }
    }
    function removeJenre(e) {
        setSelectedJenres(selectedJenres.filter((item) => item.value !== e.target.dataset.value))
    }

    //COLORS
    function MenuListColors(props) {
        return (
            <div style={{ height: '200px' }}>
                <PerfectScrollbar>
                    {props.children}
                </PerfectScrollbar>
            </div>
        )
    }
    function OptionColors(props) {
        return (
            <div className={cn('filter-option',
                { 'selected-option': selectedColors.find(item => item.value === props.value) ? true : false })}
                data-value={props.value}
                data-label={props.label}
                onClick={addColor}>
                {props.children}
            </div>
        )
    }
    const componentsColors = {
        MenuList: MenuListColors,
        Option: OptionColors
    }
    const [selectedColors, setSelectedColors] = useState([])

    function addColor(e) {
        let unique = true
        for (let i = 0; i < selectedColors.length; i++) {
            if (selectedColors[i].value === e.currentTarget.dataset.value) {
                unique = false;
                break;
            }
        }
        if (unique) {
            setSelectedColors([...selectedColors, { label: e.currentTarget.dataset.label, value: e.currentTarget.dataset.value }])
            console.log(e.currentTarget);
            e.currentTarget.classList.add('selected-option')
        }
    }
    function removeColor(e) {
        setSelectedColors(selectedColors.filter((item) => item.value !== e.target.dataset.value))
    }


    const [allFiltersOpened, setAllFiltersOpened] = useState(true)
    function resetFilters() {
        setPriceMin(100000)
        setPriceMax(900000)
        setSizeMin(2)
        setSizeMax(4)
        setSelectedAuthors([])
        setSelectedJenres([])
        setSelectedColors([])
    }
    //prices
    const [priceMin, setPriceMin] = useState(100000)
    const [priceMax, setPriceMax] = useState(900000)
    function setPrice(e) {
        setPriceMin(e[0])
        setPriceMax(e[1])
    }
    function setCorrectPrice(e) {
        if (e.currentTarget.id === 'price-from' && priceMin >= priceMax) {
            setPriceMin(priceMax - 1)
        } else if (e.currentTarget.id === 'price-to' && priceMax <= priceMin) {
            setPriceMax(priceMin + 1)
        }
    }
    function setPriceFromInput(e) {
        if (e.currentTarget.id === 'price-from') {
            if (e.currentTarget.value.length === 1 && e.nativeEvent.data === '0') {
                e.currentTarget.value = ''
                setPriceMin('')
            } else {
                setPriceMin(e.currentTarget.value)
            }
        } else if (e.currentTarget.id === 'price-to') {
            if (e.currentTarget.value.length === 1 && e.nativeEvent.data === '0') {
                e.currentTarget.value = ''
                setPriceMax('')
            } else {
                setPriceMax(e.currentTarget.value)
            }
        }
    }

    //sizes
    const [sizeMin, setSizeMin] = useState(2)
    const [sizeMax, setSizeMax] = useState(4)
    function setSize(e) {
        setSizeMin(e[0])
        setSizeMax(e[1])
    }

    //tabs
    const [activeTab, setActiveTab] = useState('author')
    const [tabsOpened, setTabsOpened] = useState(true)
    function toggleActiveTab(e) {
        setActiveTab(e.currentTarget.dataset.name)
    }
    function toggleTabsOpened() {
        setTabsOpened(tabsOpened => !tabsOpened)
    }

    return (
        <div className="catalog-filters-wrapper"><a className="catalog-filters__back">Назад</a>
            <div className="container row">
                <button onClick={() => { setAllFiltersOpened(!allFiltersOpened) }} className="catalog-filters__minimize js-minimize"><svg className="options-filter" width="17" height="15" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 2H16.5M0 9H16.5M0 16H16.5" stroke="black" />
                    <circle className="c1" cx="8" cy="2" r="1.5" fill="white" stroke="black" />
                    <circle className="c3" cx="8" cy="9" r="1.5" fill="white" stroke="black" />
                    <circle className="c2" cx="8" cy="16" r="1.5" fill="white" stroke="black" />
                </svg><span>Фильтры</span><svg className='arrow-filter' viewBox="0 0 9 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.5 0.5L4.5 4.5L8.5 0.5" stroke="black" />
                    </svg>
                </button>
            </div>
            {allFiltersOpened && <form className="catalog-filters js-catalog-filters">
                <div className="container relative js-sliders-block">
                    <div className="catalog-filters__sliders sliders">
                        <div className="sliders__item sliders__item--price">
                            <div className="sliders__price-inputs">
                                <input className="sliders__input-from" id="price-from" type="number" value={priceMin} onInput={setPriceFromInput} onBlur={setCorrectPrice} /><span className="dash"></span>
                                <input className="sliders__input-to" id="price-to" type="number" value={priceMax} onInput={setPriceFromInput} onBlur={setCorrectPrice} />
                            </div>
                            <span className="sliders__label-text sliders__label-text--price">Цена:</span>
                            <Range allowCross={false} min={0} max={1000000} marks={{ 0: 0, 1000000: 'max' }} value={[priceMin, priceMax]} onChange={setPrice} />
                            {/* <input className="js-range-slider" id="price-slider" type="text" data-type="double" data-grid="true" data-grid-num="1" data-min="5000" data-max="100000" /> */}
                        </div>
                        <label className="sliders__item sliders__item--size">
                            <span className="sliders__label-text sliders__label-text--size">Размер:</span>
                            {/* <input className="js-range-slider" id="size-slider" type="text" data-type="double" data-grid="true" grid-snap="true" data-min="0" data-max="4" data-values="S, M, L, XL, XXL" data-grid-snap="true" /> */}
                            <Range allowCross={false} min={1} max={5} marks={{ 1: 'S', 2: 'M', 3: 'L', 4: 'XL', 5: 'XXL' }} defaultValue={[2, 4]} onChange={setSize} value={[sizeMin, sizeMax]} />
                        </label>
                        <div className="sliders__orientation">
                            <span className="sliders__orientation-text">Ориентация:</span>
                            <label className="sliders__orientation-label">
                                <input className="sliders__orientation-checkbox visually-hidden" type="checkbox" name="quadro" /><span className="icon-quadro"> </span>
                            </label>
                            <label className="sliders__orientation-label">
                                <input className="sliders__orientation-checkbox visually-hidden" type="checkbox" name="portrait" /><span className="icon-portrait"> </span>
                            </label>
                            <label className="sliders__orientation-label">
                                <input className="sliders__orientation-checkbox visually-hidden" type="checkbox" name="album" /><span className="icon-album"></span>
                            </label>
                        </div>
                    </div>
                    <label className="catalog-filters__reset">
                        <svg className="reset-icon">
                            <use xlinkHref="#reset"></use>
                        </svg>
                        <input className="catalog-filters__reset-button js-input-reset" onClick={resetFilters} type="reset" value="Сбросить фильтр" />
                    </label>
                </div>
                <div className="catalog-tabs container container--zero-padding-mobile js-catalog-tabs">
                    <div className={cn('tabs-minimize', 'animated-arrow', { 'active': tabsOpened })} onClick={toggleTabsOpened}>{tabsOpened ? 'Свернуть фильтр' : 'Развернуть фильтр'}</div>
                    {tabsOpened && <div className="tabs-wrapper js-tabs-wrapper" data-opened="0" data-media="767">
                        <div className="container relative">
                            <div className="tabs-header js-tabs-header">
                                <div className={cn('tabs-title', 'animated-arrow', { 'active': activeTab === 'author' })} onClick={toggleActiveTab} data-name='author'>Автор</div>
                                <div className={cn('tabs-title', 'animated-arrow', { 'active': activeTab === 'jenre' })} onClick={toggleActiveTab} data-name='jenre'>Жанр</div>
                                <div className={cn('tabs-title', 'animated-arrow', { 'active': activeTab === 'color' })} onClick={toggleActiveTab} data-name='color'>Цвет</div>
                                <div className={cn('tabs-title', 'animated-arrow', { 'active': activeTab === 'theme' })} onClick={toggleActiveTab} data-name='theme'>Тема</div>
                                <div className={cn('tabs-title', 'animated-arrow', { 'active': activeTab === 'material' })} onClick={toggleActiveTab} data-name='material'>Материал</div>
                                <div className={cn('tabs-title', 'animated-arrow', { 'active': activeTab === 'style' })} onClick={toggleActiveTab} data-name='style'>Стиль</div>
                            </div>
                        </div>
                        <div className="tabs-body js-tabs-body">
                            <div className="container container--zero-padding-mobile">
                                <div className="js-tabs-item">
                                    <div className="tabs-subtitle js-tabs-subtitle tabs-subtitle--bg" onClick={toggleActiveTab} data-name='author'>Автор</div>
                                    <div className={cn('tabs-content', 'tabs-content--bg', { 'none': activeTab !== 'author' })}>
                                        <div className="autocomplete js-autocomplete">
                                            <div className="autocomplete__head">
                                                <label>Начните вводить имя автора</label>
                                                <Select
                                                    components={componentsAuthors}
                                                    styles={selectStyles}
                                                    options={authorOptions}
                                                    placeholder='...'
                                                // menuIsOpen={true} 
                                                />
                                            </div>
                                            <div className="autocomplete__options js-autocomplete-options">
                                                {selectedAuthors.map(({ label, value }, index) => {
                                                    return <div key={index} className='autocomplete__item' data-value={value} onClick={removeAuthor} onKeyUp={(e) => { if (e.code === 'Enter') removeAuthor(e) }} tabIndex='0'>{label}</div>
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="js-tabs-item" >
                                    <div className="tabs-subtitle js-tabs-subtitle" onClick={toggleActiveTab} data-name='jenre'>Жанр</div>
                                    <div className={cn('tabs-content', { 'none': activeTab !== 'jenre' })}>
                                        <div className="autocomplete js-autocomplete">
                                            <div className="autocomplete__head">
                                                <label>Начните вводить название жанра</label>
                                                <Select
                                                    styles={selectStyles}
                                                    options={jenreOptions}
                                                    placeholder='...'
                                                    components={componentsJenres}
                                                />
                                            </div>
                                            <div className="autocomplete__options js-autocomplete-options">
                                                {selectedJenres.map(({ label, value }, index) => {
                                                    return <div key={index} className='autocomplete__item' data-value={value} onClick={removeJenre} onKeyUp={(e) => { if (e.code === 'Enter') removeJenre(e) }} tabIndex='0'>{label}</div>
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="js-tabs-item">
                                    <div className="tabs-subtitle js-tabs-subtitle" onClick={toggleActiveTab} data-name='color'>Цвет</div>
                                    <div className={cn('tabs-content', { 'none': activeTab !== 'color' })}>
                                        <div className="autocomplete js-autocomplete">
                                            <div className="autocomplete__head">
                                                <label>Начните вводить цвет</label>
                                                <Select
                                                    styles={selectStyles}
                                                    options={colorOptions}
                                                    placeholder='...'
                                                    components={componentsColors}
                                                />
                                            </div>
                                            <div className="autocomplete__options js-autocomplete-options">
                                                {selectedColors.map(({ label, value }, index) => {
                                                    return <div key={index} className='autocomplete__item' data-value={value} onClick={removeColor} onKeyUp={(e) => { if (e.code === 'Enter') removeColor(e) }} tabIndex='0'>{label}</div>
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="js-tabs-item">
                                    <div className="tabs-subtitle js-tabs-subtitle" onClick={toggleActiveTab} data-name='theme'>Тема</div>
                                    <div className={cn('tabs-content', { 'none': activeTab !== 'theme' })}>
                                        <div className="checkbox-items">
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="checkbox" /><span className="input checkbox-text">Война</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="checkbox" /><span className="input checkbox-text">Деревенская жизнь</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="checkbox-input" type="checkbox" /><span className="input checkbox-text">Море</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="checkbox" /><span className="input checkbox-text">Техника</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="checkbox" /><span className="input checkbox-text">Природа</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="js-tabs-item">
                                    <div className="tabs-subtitle js-tabs-subtitle" onClick={toggleActiveTab} data-name='material'>Материал</div>
                                    <div className={cn('tabs-content', { 'none': activeTab !== 'material' })}>
                                        <div className="checkbox-items">
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="material" /><span className="input checkbox-text">Холст</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="material" /><span className="input checkbox-text">Картон</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="checkbox-input" type="radio" name="material" /><span className="input checkbox-text">Бумага</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="material" /><span className="input checkbox-text">Дерево</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="material" /><span className="input checkbox-text">Камень</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="js-tabs-item">
                                    <div className="tabs-subtitle js-tabs-subtitle" onClick={toggleActiveTab} data-name='style'>Стиль</div>
                                    <div className={cn('tabs-content', 'tabs-content--border-bottom', { 'none': activeTab !== 'style' })}>
                                        <div className="checkbox-items">
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="artstyle" /><span className="input checkbox-text">Модернизм</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="artstyle" /><span className="input checkbox-text">Реализм</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="checkbox-input" type="radio" name="artstyle" /><span className="input checkbox-text">Кубизм</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="artstyle" /><span className="input checkbox-text">Абстракция</span>
                                            </label>
                                            <label className="checkbox-label">
                                                <input className="visually-hidden checkbox-input" type="radio" name="artstyle" /><span className="input checkbox-text">Портрет</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="submit-wrapper">
                                    <button className="submit-button">Показать картины</button>
                                </div>
                            </div>
                        </div>
                    </div>}
                </div>
            </form>}
        </div >
    )
}

export default CatalogFilters
