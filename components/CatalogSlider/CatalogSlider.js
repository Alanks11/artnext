import './CatalogSlider.scss'
import React from 'react';
import SwiperCore, { Navigation, Virtual } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

SwiperCore.use([Navigation, Virtual]);

const sliderConfig = {
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 15,
    loop: true,
    breakpoints: {
        1280: {
            slidesPerView: 5,
        },
        768: {
            slidesPerView: 4,
        }
    },
    navigation: {
        nextEl: '.catalog-slider .js-next',
        prevEl: '.catalog-slider .js-prev',
    },
}

function CatalogSlider({ slides }) {
    return (
        <section className="catalog-slider">
            <div className="container relative">
                <h2 className="catalog-slider__title">Популярные фильтры</h2>
                <div className="catalog-slider__slider-controls">
                    <div className="catalog__prev js-prev slider-control-btn slider-control-btn--prev slider-control-btn--no-bg"></div>
                    <div className="catalog__next js-next slider-control-btn slider-control-btn--no-bg"></div>
                </div>
                <Swiper {...sliderConfig} className="catalog-slider__container swiper-container">
                    {slides.map(({ text, imgPath }, index) => {
                        return <SwiperSlide key={index} className="catalog-slider__slide swiper-slide">
                            <a className="catalog-slider__item">
                                <div className="catalog-slider__img" style={{ backgroundImage: `url("${imgPath}")` }}></div>
                                <p className="catalog-slider__jenre">{text}</p>
                            </a>
                        </SwiperSlide>
                    })}
                </Swiper>
            </div>
        </section>
    )
}

export default CatalogSlider
