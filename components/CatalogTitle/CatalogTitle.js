import './CatalogTitle.scss'

function CatalogTitle() {
    return (
        <div className="container container--zero-padding-tablet container--zero-padding-mobile">
            <div className="catalog-title__wrapper">
                <div className=" catalog-title" style={{ backgroundImage: "url('img/catalog-title.jpg')" }}></div>
                <h2 className="catalog-title__text js-title">Каталог картин</h2>
            </div>
        </div>
    )
}

export default CatalogTitle
