import './Footer.scss'

function Footer() {
    return (
        <footer className="footer">
            <div className="footer__top footer-top">
                <div className="container">
                    <div className="footer-top__row">
                        <div className="footer-top__info">
                            <div className="footer-top__title">Об Artgallery</div>
                            <ul className="footer-top__info-list">
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">О проекте</a></li>
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Блог</a></li>
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Авторы</a></li>
                            </ul>
                        </div>
                        <div className="footer-top__info">
                            <div className="footer-top__title">Покупателям</div>
                            <ul className="footer-top__info-list">
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Доставка</a></li>
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Оплата</a></li>
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Возврат</a></li>
                            </ul>
                        </div>
                        <div className="footer-top__info">
                            <div className="footer-top__title">Каталог</div>
                            <ul className="footer-top__info-list">
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Картины</a></li>
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Коллажи</a></li>
                                <li className="footer-top__info-item"><a className="footer-top__info-link" href="#">Иллюстрации</a></li>
                            </ul>
                        </div>
                        <div className="footer-top__contacts">
                            <div className="footer-top__title">Контакты</div>
                            <div className="footer-top__contact-item">
                                <svg className="footer-top__contact-icon">
                                    <use xlinkHref="#phone"></use>
                                </svg><a className="footer-top__contact-phone" href="tel:+799912312344">+7 (999) 123 123 44</a>
                            </div>
                            <div className="footer-top__contact-item">
                                <svg className="footer-top__contact-icon">
                                    <use xlinkHref="#mail"></use>
                                </svg><a className="footer-top__contact-email" href="mailto:info@domainname.ru">info@domainname.ru</a>
                            </div>
                            <div className="footer-top__contact-item">
                                <svg className="footer-top__contact-icon">
                                    <use xlinkHref="#pin"></use>
                                </svg>
                                <div className="footer-top__contact-address">г. Москва, <br /> Преснесенская наб., 12</div>
                            </div>
                            <div className="footer-top__pay1"><img className="footer-top__img js-pay-img" src="img/pay_systems.png" alt="Платежные системы" /></div>
                        </div>
                        <div className="footer-top__column">
                            <div className="footer-top__social">
                                <div className="footer-top__title">Мы в социальных сетях:</div>
                                <div className="footer-top__social-links">
                                    <div className="footer-top__social-item"><a className="footer-top__social-link" href="#">
                                        <svg className="footer-top__social-icon">
                                            <use xlinkHref="#vk"></use>
                                        </svg></a></div>
                                    <div className="footer-top__social-item"><a className="footer-top__social-link" href="#">
                                        <svg className="footer-top__social-icon">
                                            <use xlinkHref="#fb"></use>
                                        </svg></a></div>
                                    <div className="footer-top__social-item"><a className="footer-top__social-link" href="#">
                                        <svg className="footer-top__social-icon">
                                            <use xlinkHref="#odnokl"></use>
                                        </svg></a></div>
                                    <div className="footer-top__social-item"><a className="footer-top__social-link" href="#">
                                        <svg className="footer-top__social-icon">
                                            <use xlinkHref="#inst"></use>
                                        </svg></a></div>
                                </div >
                            </div >
                            <div className="footer-top__pay">
                                <div className="footer-top__title">Платежные системы</div>
                                <div className="footer-top__pay2"><img className="footer-top__img js-pay-img" src="img/pay_systems.png" alt="Платежные системы" /></div>
                            </div>
                        </div >
                    </div >
                </div >
            </div >
            <div className="footer__bottom footer-bottom">
                <div className="footer-bottom__copy">
                    <p className="footer-bottom__copy-text">&copy; Artgallery, 2020</p>
                </div>
                <div className="container container--zero-padding-mobile">
                    <div className="footer-bottom__row">
                        <ul className="footer-bottom__links">
                            <li className="footer-bottom__item"><a className="footer-bottom__link" href="#">Политика конфиденциальности</a></li>
                            <li className="footer-bottom__item"><a className="footer-bottom__link" href="#">Условия и положения</a></li>
                        </ul>
                        <div className="footer-bottom__logo"><a className="footer-bottom__logo" href="#"><img className="footer-bottom__logo-img" src="img/logo_developer.png" alt="Хоукинг Бразерс" /></a>
                            <p className="footer-bottom__logo-text">Разработка сайта</p>
                        </div>
                        <p className="footer-bottom__sign">Сайт создан <a className="footer-bottom__sign-link" href="#">Hawking Brothers    </a></p>
                    </div>
                </div>
            </div>
        </footer >
    )
}
export default Footer
