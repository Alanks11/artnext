import './Header.scss'

import React, { Component } from 'react'
import cn from 'classnames'

import HeaderLang from '../HeaderLang/HeaderLang'
import HeaderUser from '../HeaderUser/HeaderUser'
import HeaderSearch from '../HeaderSearch/HeaderSearch'
import HeaderNav from '../HeaderNav/HeaderNav'
import Login from '../Login/Login'

class Header extends Component {
    constructor(props) {
        super(props);
        this.headerRef = React.createRef();
        this.langs = [];
        this.state = {
            lang: {
                opened: false,
                current: 'Ru',
            },
            user: {
                auth: false,
                opened: false,
            },
            favor: {
                count: 0,
            },
            cart: {
                count: 0,
            },
            search: {
                opened: false
            },
            menu: {
                opened: false
            },
            login: {
                opened: false
            }
        };
    }
    logout = (e) => {
        this.setState({
            user: {
                ...this.state.user,
                auth: false,
                opened: false
            }
        })
    }
    closeAllMenus = () => {
        this.setState({
            user: {
                ...this.state.user,
                opened: false,
            }
        })
        this.setState({
            search: {
                ...this.state.search,
                opened: false,
            }
        })
        this.setState({
            lang: {
                ...this.state.lang,
                opened: false,
            }
        })

    }
    openLoginWindow = () => {
        this.setState({
            login: {
                ...this.state.login,
                opened: true
            }
        })
    }
    closeLoginWindow = () => {
        this.setState({
            login: {
                ...this.state.login,
                opened: false
            }
        })
    }
    switchLang = (e) => {
        this.setState({
            lang: {
                ...this.state.lang,
                current: e.currentTarget.dataset.lang,
                opened: false,
            }
        })
    }
    toggleHeaderAssets = (e) => {
        this.setState({
            [e.currentTarget.dataset.name]: {
                ...this.state[e.currentTarget.dataset.name],
                opened: !this.state[e.currentTarget.dataset.name].opened,
            }
        })
        if (e.currentTarget.dataset.name === 'search') {
            setTimeout(function () {
                document.querySelector('.header__search-input').focus(); //react cries :(
            }, 0);
        }
    }
    clickOutHandler(e) {
        if (this.headerRef.current !== null) {
            if (this.headerRef.current !== e.target && !this.headerRef.current.contains(e.target)) {
                this.closeAllMenus()
            }
        }

    }
    componentDidMount() {
        fetch(`http://localhost:3000/api/mainLayout`)
            .then(res => res.json())
            .then(data => {
                this.langs = data.header.langs
                //TODO set states
            })
        window.addEventListener('click', this.clickOutHandler.bind(this));
    }
    componentWillUnmount() {
        window.removeEventListener('click', this.clickOutHandler.bind(this));
    }
    render() {
        return (
            <header className={cn(
                'header',
                { 'header--inverted': this.props.headerInverted }
            )} ref={this.headerRef}>
                <div className="container relative flex jcsb aic">
                    <HeaderNav toggle={this.toggleHeaderAssets} opened={this.state.menu.opened} />
                    <HeaderUser auth={this.state.user.auth} login={this.openLoginWindow} logout={this.logout} toggle={this.toggleHeaderAssets} opened={this.state.user.opened} />
                    <a className="header__stats">
                        <svg className="header__stats-icon">
                            <use xlinkHref="#favor"></use>
                        </svg>
                        <span className="header__stats-number">{this.state.favor.count}</span>
                    </a>
                    <a className="header__stats">
                        <svg className="header__stats-icon">
                            <use xlinkHref="#cart"></use>
                        </svg>
                        <span className="header__stats-number">{this.state.cart.count}</span>
                    </a>
                    <HeaderSearch opened={this.state.search.opened} toggle={this.toggleHeaderAssets} />
                    <HeaderLang switchLang={this.switchLang} currentLang={this.state.lang.current} langs={this.langs} toggle={this.toggleHeaderAssets} opened={this.state.lang.opened} />
                </div >
                {this.state.login.opened && <Login close={this.closeLoginWindow} />}
            </header >
        )
    }
}

export default Header
