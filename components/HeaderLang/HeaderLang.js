import './HeaderLang.scss'
import cn from 'classnames'

function HeaderLang(props) {
    return (
        <div className="header__lang js-header-lang">
            <div className="header__lang-current" data-name='lang' onClick={props.toggle}>{props.currentLang}</div>
            <div className={cn(
                "header__lang-menu lang-menu",
                "headerAssetsAppear",
                { "none": !props.opened },
            )}>
                <ul className="lang-menu__list">
                    {props.langs.filter(item => item !== props.currentLang)
                        .map(item => <li key={item} data-lang={item} onClick={props.switchLang} className="lang-menu__item"><a className="lang-menu__link">{item}</a></li>)
                    }
                </ul>
            </div>
        </div>
    )
}
export default HeaderLang
