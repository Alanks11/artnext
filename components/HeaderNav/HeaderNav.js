import './HeaderNav.scss'

import { useEffect, useRef } from 'react'
import Link from 'next/link'
import cn from 'classnames'
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock'

function HeaderNav(props) {
    const menuElem = useRef(null)
    useEffect(() => {
        if (window.matchMedia('(max-width: 959px)').matches) {
            !props.opened ? enableBodyScroll(menuElem) : disableBodyScroll(menuElem);
        }
        return () => { clearAllBodyScrollLocks() }
    })

    return (
        <>
            <div className={`header__hamburger ${props.opened ? ' header__hamburger--active' : ''}`} data-name="menu" onClick={props.toggle}><span></span><span></span><span></span><span></span></div>
            <Link href="/"><a className="header__logo"></a></Link>
            <nav className="header__topmenu js-header-topmenu">
                <div className="header__menu-item js-menu-item header__menu-item--top"><Link href="/catalog"><a className="header__menu-link">Каталог</a></Link></div>
                <div className="header__menu-item js-menu-item header__menu-item--top"><a className="header__menu-link" href="#">Художникам</a></div>
                <div className="header__menu-item js-menu-item header__menu-item--top"><a className="header__menu-link" href="#">Блог</a></div>
                <div className="header__menu-item header__menu-item--more" data-name="menu" onMouseEnter={props.opened ? null : props.toggle} onMouseLeave={props.toggle}>
                    <a className="header__menu-link header__menu-link--more js-more" >Еще</a>
                    <nav ref={menuElem} className={cn(
                        "header__bottommenu",
                        "headerAssetsAppear",
                        { "none": !props.opened }
                    )} >
                        <div className="header__bottommenu-wrapper">
                            <div className="container">
                                <div className="header__menu-item js-menu-item header__menu-item--bottom"><Link href="/catalog"><a className="header__menu-link" >Каталог</a></Link></div>
                                <div className="header__menu-item js-menu-item header__menu-item--bottom"><a className="header__menu-link" href="#">Художникам</a></div>
                                <div className="header__menu-item js-menu-item header__menu-item--bottom"><a className="header__menu-link" href="#">Блог</a></div>
                                <div className="header__menu-item js-menu-item header__menu-item--non-transfer"><a className="header__menu-link" href="#">О проекте</a></div>
                                <div className="header__menu-item js-menu-item header__menu-item--non-transfer header__menu-item--end-mobile"><a className="header__menu-link" href="#">Доставка</a></div>
                                <div className="header__menu-item js-menu-item header__menu-item--non-transfer"><a className="header__menu-link" href="#">Политика конфиденциальности</a></div>
                                <div className="header__menu-item js-menu-item header__menu-item--lang"><a className="header__menu-link" href="#">Английская версия</a></div>
                            </div>
                            <div className="header__copy">&copy; Artgallery, 2020</div>
                        </div>
                    </nav>
                </div>
            </nav>
        </>
    )
}
export default HeaderNav
