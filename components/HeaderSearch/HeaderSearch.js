import './HeaderSearch.scss'

import React from 'react'
import cn from 'classnames'

function HeaderSearch(props) {
    return (
        <div className="header__search">
            <svg className="header__search-icon js-header-search" data-name='search' onClick={props.toggle}>
                <use xlinkHref="#search"></use>
            </svg>
            <div className={cn(
                "header__search-window",
                "headerAssetsAppear",
                { "none": !props.opened }
            )}>
                <form className="header__search-item">
                    <input className="header__search-input js-header-search-input" type="text" placeholder="Поиск..." autoFocus="autoFocus" />
                    <div className="header__search-close js-header-search-close" data-name='search' onClick={props.toggle}></div>
                    <button className="header__search-submit js-header-search-submit">
                        <svg className="header__submit-icon">
                            <use xlinkHref="#search"></use>
                        </svg>
                    </button>
                </form>
            </div >
        </div>
    )
}
export default HeaderSearch
