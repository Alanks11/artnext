import './HeaderUser.scss'
import cn from 'classnames'

function HeaderUser(props) {
    return (
        <div className="header__user">
            <div className="header__user-inner" onClick={props.auth ? props.toggle : props.login} data-name='user'>
                <div className="header__user-imgbox"><svg className="header__guest-img" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18.7782 14.2218C17.5801 13.0237 16.1541 12.1368 14.5982 11.5999C16.2646 10.4522 17.3594 8.53136 17.3594 6.35938C17.3594 2.85282 14.5066 0 11 0C7.49345 0 4.64062 2.85282 4.64062 6.35938C4.64062 8.53136 5.73543 10.4522 7.40188 11.5999C5.84598 12.1368 4.41994 13.0237 3.22184 14.2218C1.14421 16.2995 0 19.0618 0 22H1.71875C1.71875 16.8823 5.88229 12.7188 11 12.7188C16.1177 12.7188 20.2812 16.8823 20.2812 22H22C22 19.0618 20.8558 16.2995 18.7782 14.2218ZM11 11C8.44117 11 6.35938 8.91825 6.35938 6.35938C6.35938 3.8005 8.44117 1.71875 11 1.71875C13.5588 1.71875 15.6406 3.8005 15.6406 6.35938C15.6406 8.91825 13.5588 11 11 11Z" />
                </svg>
                    {props.auth && <img className="header__user-img" src="img/alex.png" alt="Аватар" />}
                </div>
                {props.auth && <div className="header__user-name">Александрелло</div>}
            </div>
            <div className={cn(
                "header__user-menu",
                "user-menu",
                "headerAssetsAppear",
                { "none": !props.opened }
            )}>
                <ul className="user-menu__list">
                    <li className="user-menu__item"><a className="user-menu__link">Мой профиль</a></li>
                    <li className="user-menu__item"><a className="user-menu__link">Мои заказы</a></li>
                    <li className="user-menu__item user-menu__item--end"><a className="user-menu__link">Сообщения</a></li>
                    <li className="user-menu__item"><a className="user-menu__link">Настройки профиля</a></li>
                    <li className="user-menu__item user-menu__item--end"><a className="user-menu__link">Мои работы</a></li>
                </ul>
                <button className="user-menu__out" onClick={props.logout}>
                    <svg className="user-menu__out-icon">
                        <use xlinkHref="#logout"></use>
                    </svg><span className="user-menu__out-text">Выйти</span>
                </button>
            </div>
        </div>
    )
}
export default HeaderUser

