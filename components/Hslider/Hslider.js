import './Hslider.scss'

import React, { Component } from 'react'
import SwiperCore, { Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

import ButtonStd from '../ButtonStd/ButtonStd'

SwiperCore.use([Pagination]);

const hSliderConfig = {
    loop: true,
    direction: 'horizontal',
    pagination: {
        el: '.h-slider__pagination',
        type: 'bullets',
        clickable: true,
    },
}

class Hslider extends Component {
    render() {
        const hSlides = this.props.data.slides
        return (
            <Swiper {...hSliderConfig}>
                <div className="h-slider__pagination"></div>
                {hSlides.map(({ imagePath, pretitle, title, desc }) => {
                    return <SwiperSlide className="h-slide" style={{ backgroundImage: `url('${imagePath}')` }} key={imagePath}>
                        <div className="container">
                            <div className="h-slide__content">
                                <p className="h-slide__pretitle">{pretitle}</p>
                                <h2 className="h-slide__title"><span className="h-slide__title-text">{title}</span></h2>
                                <p className="h-slide__descr">{desc}</p>
                            </div>
                            <ButtonStd modClass='h-slide__button btn-std--inverted' text='Открыть' />
                        </div>
                    </SwiperSlide>
                })}
            </Swiper >
        )
    }
}
export default Hslider
