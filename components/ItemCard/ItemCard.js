import './ItemCard.scss'

import React, { useEffect, useState } from 'react'
import SwiperCore, { Thumbs, Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import cn from 'classnames'

function ItemCard({ previews, images, description }) {
    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const [slideModal, setSlideModal] = useState(false);
    const [itemParamsOpened, setItemParamsOpened] = useState(true);
    const [itemDescOpened, setItemDescOpened] = useState(true)
    const [favor, setFavor] = useState(false)

    useEffect(() => {
        let resize;
        if (typeof (Event) === 'function') {
            resize = new Event('resize');
        } else {
            resize = document.createEvent('Event');
            resize.initEvent('resize', true, true);
        }
        window.dispatchEvent(resize);
    })

    function toggleSlideModal() {
        setSlideModal(slideModal => !slideModal);
    }

    function toggleItemParamsOpened() {
        setItemParamsOpened(itemParamsOpened => !itemParamsOpened)
    }

    function toggleItemDescOpened() {
        setItemDescOpened(itemDescOpened => !itemDescOpened)
    }

    SwiperCore.use([Thumbs, Navigation]);

    const sliderConfig = {
        direction: 'horizontal',
        loop: true,
        navigation: {
            nextEl: '.slider__main-navigation .js-next',
            prevEl: '.slider__main-navigation .js-prev',
        },
        autoHeight: true,
    }
    const thumbsConfig = {
        height: 170,
        direction: 'horizontal',
        loop: true,
        slidesPerView: 3,
        slideToClickedSlide: true,
        //loopedSlides: 3,
        spaceBetween: 10,
        slidesPerGroup: 1,
        centeredSlides: false,
        breakpoints: {
            768: {
                direction: 'vertical',
                height: 275,
            }
        },
        slideToClickedSlide: true,
    }

    const { title, articul, author, size, style, price, oldPrice, year, theme, materials, aboutItem } = description
    return (
        <section className="item-card">
            <div className="container">
                <div className="item-card__inner">
                    <div className="item-card__column-left">
                        <h2 className="content__title content__title--show">{title}</h2><a className="content__author content__author--show" href="#">{author}</a>
                        <div className="item-card__slider slider">
                            <div className="slider__carousel">
                                <Swiper {...thumbsConfig} onSwiper={setThumbsSwiper}
                                    watchSlidesVisibility
                                    watchSlidesProgress
                                    className="swiper-container">
                                    {previews.map((item, index) => {
                                        return <SwiperSlide className="swiper-slide slider__carousel-imgbox" key={index}>
                                            <img className="slider__carousel-img" src={item} alt="Превью картины" />
                                        </SwiperSlide>
                                    })}
                                </Swiper>
                            </div>
                            <div className="slider__main-wrapper" className={cn('slider__main-wrapper', { 'active': slideModal })}>
                                <div className="slider__main">
                                    <div className="slider__main-modal" onClick={toggleSlideModal}></div>
                                    <Swiper {...sliderConfig} thumbs={{ swiper: thumbsSwiper }} className="swiper-container" >
                                        <div className="slider__main-navigation">
                                            <div className="js-prev prev"></div>
                                            <div className="js-next next"></div>
                                        </div>
                                        {images.map((item, index) => {
                                            return <SwiperSlide key={index} className="swiper-slide slider__main-imgbox">
                                                <img className="slider__main-img" src={item} alt="Фото картины" />
                                            </SwiperSlide>
                                        })}
                                    </Swiper>
                                    <div className="item-card__actions actions">
                                        <button className="actions__item actions__item--liked" data-liked="(875)">
                                            <span className="actions__text">Понравилось
                                                <svg className="actions__icon">
                                                    <use xlinkHref="#like"></use>
                                                </svg>
                                            </span>
                                        </button>
                                        <a className="actions__item actions__item--right" href="#" download="download">
                                            <span className="actions__text">Цифровая копия
                                                <svg className="actions__icon">
                                                    <use xlinkHref="#download"></use>
                                                </svg>
                                            </span>
                                        </a>
                                        <button className="actions__item">
                                            <span className="actions__text">Больше не показывать картину
                                                <svg className="actions__icon">
                                                    <use xlinkHref="#visibility"></use>
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item-card__column-right">
                        <div className="item-card__content content">
                            <h2 className="content__title content__title--hide">{title}</h2>
                            <div className="content__assets assets"><span className="assets__code">{articul}</span>
                                <button className={cn("assets__favor", { "active": favor })} onClick={() => { setFavor(true) }}>
                                    <svg className="assets__favor-icon">
                                        <use xlinkHref="#favor"></use>
                                    </svg>
                                </button>
                            </div><a className="content__author content__author--hide" href="#">{author}</a>
                            <div className="content__info info">
                                <div className="info__item"><span className="info__key">Размер картины: </span><span className="info__value">{size}</span></div>
                                <div className="info__item"><span className="info__key">Стиль: </span><a className="info__value info__value--link" href="#">{style}</a></div>
                            </div>
                            <div className="content__purchase purchase">
                                <div className="purchase__price"><span className="purchase__price-new">{price} <span className='ruble'>7</span></span><span className="purchase__price-old">{oldPrice}</span></div>
                                <button className="purchase__button big-button"></button>
                            </div>
                            <div className="content__buttons buttons">
                                <button className="buttons__item">
                                    <span className="buttons__item-text">Понравилось
                                        <svg className="buttons__item-icon">
                                            <use xlinkHref="#like"></use>
                                        </svg>
                                    </span>
                                </button>
                                <button className="buttons__item">
                                    <span className="buttons__item-text">В избранное
                                        <svg className="buttons__item-icon" fill="none" stroke="black">
                                            <use xlinkHref="#favor"></use>
                                        </svg>
                                    </span>
                                </button>
                                <button className="buttons__item">
                                    <span className="buttons__item-text">Не показывать
                                        <svg className="buttons__item-icon">
                                            <use xlinkHref="#visibility"></use>
                                        </svg>
                                    </span>
                                </button>
                                <a className="buttons__item" href="#" download="download">
                                    <span className="buttons__item-text">Скачать копию
                                        <svg className="buttons__item-icon">
                                            <use xlinkHref="#download"></use>
                                        </svg>
                                    </span>
                                </a>
                            </div>
                            <div className="content__social"><a className="content__social-link" href="#">
                                <svg className="content__social-icon">
                                    <use xlinkHref="#vk"></use>
                                </svg></a><a className="content__social-link" href="#">
                                    <svg className="content__social-icon">
                                        <use xlinkHref="#fb"></use>
                                    </svg></a><a className="content__social-link" href="#">
                                    <svg className="content__social-icon">
                                        <use xlinkHref="#odnokl"></use>
                                    </svg></a><a className="content__social-link" href="#">
                                    <svg className="content__social-icon">
                                        <use xlinkHref="#mail"></use>
                                    </svg></a></div>
                            <div className="content__descr descr">
                                <h3 className={cn('descr__title', 'animated-arrow', { 'active': itemParamsOpened })} onClick={toggleItemParamsOpened}>Параметры картины</h3>
                                <div className={cn('js-descr-content', { 'hidden': !itemParamsOpened })} >
                                    <table className="descr__content">
                                        <tbody>
                                            <tr>
                                                <td>Год написания</td>
                                                <td>{year}</td>
                                            </tr>
                                            <tr>
                                                <td>Тема</td>
                                                <td>{theme}</td>
                                            </tr>
                                            <tr>
                                                <td>Стиль</td>
                                                <td>{style}</td>
                                            </tr>
                                            <tr>
                                                <td>Материалы</td>
                                                <td>{materials}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h3 className={cn('descr__title', 'animated-arrow', { 'active': itemDescOpened })} onClick={toggleItemDescOpened}>Описание картины</h3>
                                <p className={cn('js-descr-content', 'descr__content-text', { 'hidden': !itemDescOpened })}>{ }{aboutItem}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ItemCard
