import './ItemWorks.scss'

import React from 'react'
import SwiperCore, { Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

import TitleStd from '../TitleStd/TitleStd'
import NewWorkItem from '../NewWorkItem/NewWorkItem'

SwiperCore.use([Navigation]);

const itemWorksSliderConfig = {
    slidesPerView: 2,
    spaceBetween: 10,
    loop: false,
    navigation: {
        nextEl: '.item-works .js-next',
        prevEl: '.item-works .js-prev',
    },
    breakpoints: {
        1280: {
            slidesPerView: 4,
            spaceBetween: 45,
        },

    },
}

function ItemWorks({ sameWorks }) {
    return (
        <section className="item-works lazy-section">
            <TitleStd text='Похожие работы' modClass='item-works__title' />
            <div className="container relative">
                <div className="item-works__slider-controls">
                    <div className="js-prev slider-control-btn slider-control-btn--prev"></div>
                    <div className="js-next slider-control-btn "></div>
                </div>
                <Swiper {...itemWorksSliderConfig} className="item-works__container swiper-container">
                    {sameWorks.map((item, ind, arr) => {
                        return <SwiperSlide key={ind}>
                            <NewWorkItem data={arr[ind]} />
                        </SwiperSlide>
                    })}
                </Swiper>
            </div>
        </section>
    )
}

export default ItemWorks
