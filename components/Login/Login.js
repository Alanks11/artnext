import './Login.scss'

import { useState } from 'react'
import { useClickOut } from '../../customHooks'

import LoginPassRecovery from '../LoginPassRecovery/LoginPassRecovery'
import LoginAuth from '../LoginAuth/LoginAuth'
import LoginReg from '../LoginReg/LoginReg'
import LoginSuccess from '../LoginSuccess/LoginSuccess'

function Login({ close }) {

    const [refModal, refClose] = useClickOut(
        null,
        close
    )

    const stages = {
        reg: <LoginReg toggle={setActualStage} />,
        auth: <LoginAuth toggle={setActualStage} />,
        passRecovery: <LoginPassRecovery toggle={setActualStage} success={success} />,
        success: <LoginSuccess />
    }
    const [currentStage, setCurrentStage] = useState(stages['auth'])

    function setActualStage(e) {
        setCurrentStage(stages[e.currentTarget.dataset.stage])
    }

    function success() {
        setCurrentStage(stages['success'])
    }

    return (
        <div className="header__popups popups headerAssetsAppear" >
            <div className="popups__window" ref={refModal}>
                <div className="popups__close" onClick={close} ref={refClose}></div>
                {currentStage}
            </div >
        </div >
    )

}
export default Login
