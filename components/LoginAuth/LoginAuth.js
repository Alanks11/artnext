import './LoginAuth.scss'

import cn from 'classnames'
import { useCheckInput } from '../../customHooks'

function LoginAuth(props) {

    const password = useCheckInput((value) => { return value.trim().length > 8 })
    const email = useCheckInput((value) => { return (/.+@.+\..+/.test(value)) })

    function submit(e) {
        e.preventDefault()
        let pass = password.validate()
        let mail = email.validate()
        if (pass && mail) {
            console.log('OK');
            //some ajax
        }
    }

    return (
        <div className="popups__auth auth popups__item headerAssetsAppear">
            <div className="popup__header">
                <div className="popup__title">Авторизация</div>
                <div className="popup-aside hide-mobile"><span>Еще нет аккаунта?</span>
                    <button className="js-go-to-reg" data-stage='reg' onClick={props.toggle}>Зарегистрироваться</button>
                </div>
            </div>
            <div className="popup__body">
                <div className="popup__social">
                    <p className="popup__social-text">С помощью социальных сетей</p>
                    <a className="popup__social-link" href="#">VKontakte
                        <svg className="popup__social-icon">
                            <use xlinkHref="#vk"></use>
                        </svg>
                    </a>
                    <a className="popup__social-link" href="#">Facebook
                        <svg className="popup__social-icon">
                            <use xlinkHref="#fb"></use>
                        </svg>
                    </a>
                    <a className="popup__social-link" href="#">Olnoklassniki
                        <svg className="popup__social-icon">
                            <use xlinkHref="#odnokl"></use>
                        </svg>
                    </a>
                    <a className="popup__social-link" href="#">Google+
                        <svg className="popup__social-icon">
                            <use xlinkHref="#google"></use>
                        </svg>
                    </a >
                </div >
                <form className="auth__form js-form-auth" action="#" method="POST" noValidate={true} onSubmit={submit}>
                    <label className={cn("form-item", { "error": email.inputError })}>
                        <span className={cn("form-text", { "error": email.inputError })}>E-mail</span>
                        <input className={cn("form-input", { "error": email.inputError })} type="email" name="email" required="required" onInput={email.setValue} value={email.inputValue} />
                        {email.inputError && <span>Некорректный адрес почты</span>}
                    </label>
                    <label className={cn("form-item", { "error": password.inputError })}>
                        <span className={cn("form-text", { "error": password.inputError })}>Пароль</span>
                        <input className={cn("form-input", { "error": password.inputError })} type="password" name="password" required="required" onInput={password.setValue} value={password.inputValue} />
                        {password.inputError && <span>Длина пароля - не менее 8 символов</span>}
                    </label>
                    <button className="form-submit big-button">Авторизоваться</button>
                    <div className="popup-aside popup-aside--center"><span className="hide-mobile">Забыли пароль?</span><a className="js-go-to-reg show-mobile" data-stage='reg' onClick={props.toggle}>Зарегистрироваться </a><a className="js-go-to-pass" data-stage='passRecovery' onClick={props.toggle}>Восстановить пароль</a></div>
                </form>
            </div >
        </div >
    )
}
export default LoginAuth
