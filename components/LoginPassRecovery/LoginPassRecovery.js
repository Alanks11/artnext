import './LoginPassRecovery.scss'

import React from 'react'
import cn from 'classnames'
import { useCheckInput } from '../../customHooks'


function LoginPassRecovery(props) {
    const email = useCheckInput((value) => { return (/.+@.+\..+/.test(value)) })

    function submit(e) {
        e.preventDefault()
        if (email.validate()) {
            console.log('OK');
            //some ajax
            setTimeout(function () {
                props.success()
            }, 1000);
        }
    }
    return (
        <div className="popups__pass pass popups__item popups__item--small headerAssetsAppear">
            <div className="popup__header popup__header--center">
                <div className="popup__title" data-stage='passRecovery' onClick={props.toggle}>Восстановить пароль</div>
            </div>
            <form className="pass__content js-form-pass" action="#" method="POST" noValidate={true} onSubmit={submit}>
                <label className="form-item">
                    <span className={cn("form-text", { "error": email.inputError })}>E-mail</span>
                    <input className={cn("form-input", { "error": email.inputError })} type="email" name="email" required="required" onInput={email.setValue} value={email.inputValue} />
                    {email.inputError && <span>Некорректный адрес почты</span>}
                </label>
                <button className="form-submit big-button" data-stage='passRecovery' onClick={props.toggle}>Восстановить пароль</button>
                <div className="popup-aside popup-aside--center"><span>Вспомнили пароль?</span><a className="js-go-to-auth" data-stage='auth' onClick={props.toggle}> Авторизоваться</a></div>
            </form>
        </div>
    )
}
export default LoginPassRecovery
