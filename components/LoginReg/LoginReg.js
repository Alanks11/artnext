import './LoginReg.scss'

import React, { useState } from 'react'
import { useCheckInput } from '../../customHooks'
import cn from 'classnames'

function LoginReg(props) {
    const password = useCheckInput((value) => { return value.trim().length > 8 })
    const email = useCheckInput((value) => { return (/.+@.+\..+/.test(value)) })
    const name = useCheckInput((value) => { return value.trim().length > 0 })
    const surname = useCheckInput((value) => { return value.trim().length > 0 })
    const [agree, setAgree] = useState(false)

    function submit(e) {
        e.preventDefault()
        let pass = password.validate()
        let mail = email.validate()
        let nm = name.validate()
        let snm = surname.validate()
        if (pass && mail && nm && snm && agree) {
            console.log('OK');
            //some ajax
        }
    }
    return (
        <div className="popups__reg reg popups__item headerAssetsAppear">
            <div className="popup__header popup__header--padding-bottom">
                <div className="popup__title">Регистрация<span className="popup__subtitle">Это бесплатно и займет не более 3 минут</span></div>
                <div className="popup-aside popup-aside--bottom"><span>Уже зарегистрированы?</span>
                    <button className="js-go-to-auth" data-stage='auth' onClick={props.toggle}>Авторизоваться</button>
                </div>
            </div>
            <form className="reg__form js-form-reg" action="#" method="POST" noValidate={true} onSubmit={submit}>
                <div className="reg__choice">
                    <label className="reg__choice-label">
                        <input className="reg__choice-check visually-hidden" type="radio" name="choice" checked="checked" /><span className="reg__choice-text">Я художник</span>
                    </label>
                    <label className="reg__choice-label">
                        <input className="reg__choice-check visually-hidden" type="radio" name="choice" /><span className="reg__choice-text">Я покупатель</span>
                    </label>
                </div>
                <div className="popup__body">
                    <div className="popup__social">
                        <p className="popup__social-text">С помощью социальных сетей</p>
                        <a className="popup__social-link" href="#">VKontakte
                            <svg className="popup__social-icon">
                                <use xlinkHref="#vk"></use>
                            </svg>
                        </a>
                        <a className="popup__social-link" href="#">Facebook
                            <svg className="popup__social-icon">
                                <use xlinkHref="#fb"></use>
                            </svg>
                        </a>
                        <a className="popup__social-link" href="#">Olnoklassniki
                            <svg className="popup__social-icon">
                                <use xlinkHref="#odnokl"></use>
                            </svg>
                        </a>
                        <a className="popup__social-link" href="#">Google+
                            <svg className="popup__social-icon">
                                <use xlinkHref="#google"></use>
                            </svg>
                        </a>
                    </div>
                    <div className="reg__inputs">
                        <p className="reg__text hide-mobile">С помощью E-mail</p>
                        <div className="reg__userdata">
                            <label className={cn("form-item", { "error": name.inputError })}>
                                <span className={cn("form-text", { "error": name.inputError })}>Имя</span>
                                <input className={cn("form-input", { "error": name.inputError })} type="text" name="name" required="required" onInput={name.setValue} value={name.inputValue} />
                                {name.inputError && <span>Длина имени - не менее 1 символа</span>}
                            </label>
                            <label className={cn("form-item", { "error": surname.inputError })}>
                                <span className={cn("form-text", { "error": surname.inputError })}>Фамилия</span>
                                <input className={cn("form-input", { "error": surname.inputError })} type="text" name="surname" required="required" onInput={surname.setValue} value={surname.inputValue} />
                                {surname.inputError && <span>Длина фамилии - не менее 1 символа</span>}
                            </label>
                        </div>
                        <label className={cn("form-item", { "error": email.inputError })}>
                            <span className={cn("form-text", { "error": email.inputError })}>E-mail</span>
                            <input className={cn("form-input", { "error": email.inputError })} type="email" name="email" required="required" onInput={email.setValue} value={email.inputValue} />
                            {email.inputError && <span>Некорректный адрес почты</span>}
                        </label>
                        <label className={cn("form-item", { "error": password.inputError })}>
                            <span className={cn("form-text", { "error": password.inputError })}>Пароль</span>
                            <input className={cn("form-input", { "error": password.inputError })} type="password" name="password" required="required" onInput={password.setValue} value={password.inputValue} />
                            {password.inputError && <span>Длина пароля - не менее 8 символов</span>}
                        </label>
                        <label className="reg__agree">
                            <input className={cn("visually-hidden", "reg__agree-input", { "error": !agree })} type="checkbox" name="agree" required="required" checked={agree ? true : false} onInput={() => { setAgree(!agree) }} />
                            <span className={cn("reg__agree-text", { "error": !agree })}>Я согласен с <a href="#" className="reg__agree-link">пользовательским соглашением</a>
                            </span>
                        </label>
                        <button className="form-submit big-button">Зарегистрироваться</button>
                    </div>
                </div>
            </form >
        </div >
    )
}
export default LoginReg
