import './LoginSuccess.scss'

import React from 'react'

function LoginSuccess() {
    return (
        <div className="popups__success success popups__item popups__item--small">
            <div className="popup__header popup__header--center">
                <div className="popup__title">Успех!</div>
            </div>
            <p className="success__text">Мы отправили на ваш e-mail инструкцию по сбросу пароля</p>
            <svg className="success__img">
                <use xlinkHref="#success"></use>
            </svg>
        </div >
    )
}
export default LoginSuccess
