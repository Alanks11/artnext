import React, { useEffect, useState } from 'react';
import { MainContext } from '../../MainContext'

import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import SvgSprite from '../SvgSprite/SvgSprite'

function MainLayout({ children, headerInverted }) {
    const [headerShow, setHeaderShow] = useState(true);
    function toggleHeaderShow() {
        setHeaderShow(headerShow => !headerShow);
    }
    const [desktop, setDesktop] = useState(false)
    const [tablet, setTablet] = useState(false)
    const [mobile, setMobile] = useState(false)

    function defineDevice() {
        if (window.matchMedia('(min-width: 1280px)').matches) {
            setDesktop(true)
            setTablet(false)
            setMobile(false)
        }
        if (window.matchMedia('(max-width: 1279px) and (min-width: 768px').matches) {
            setDesktop(false)
            setTablet(true)
            setMobile(false)
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            setDesktop(false)
            setTablet(false)
            setMobile(true)
        }
    }
    useEffect(() => {
        defineDevice()
        window.addEventListener('resize', defineDevice);
        return () => {
            window.removeEventListener('resize', defineDevice);
        }
    })
    return (<>
        <MainContext.Provider value={{
            headerShow: headerShow,
            toggleHeaderShow: toggleHeaderShow,
            desktop: desktop,
            tablet: tablet,
            mobile: mobile
        }}>
            <SvgSprite />
            <div className="wrapper">
                {headerShow && <Header headerInverted={headerInverted} />}
                {children}
                <Footer />
            </div >
        </MainContext.Provider>
    </>
    )
}

export default MainLayout
