import './NewWorkChoice.scss'

function NewWorkChoice() {
    return (
        <div className="new-works-section__choice choice">
            <img className="choice__img choice__img--1" src="img/choice1.png" alt="Декор" />
            <img className="choice__img choice__img--2" src="img/choice2.png" alt="Декор" />
            <img className="choice__img choice__img--3" src="img/choice3.png" alt="Декор" />
            <img className="choice__img choice__img--4" src="img/choice4.png" alt="Декор" />
            <a className="choice__title">Умный выбор по параметрам с примерами</a><a className="choice__button">Подобрать</a>
        </div>
    )
}
export default NewWorkChoice
