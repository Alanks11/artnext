import './NewWorkItem.scss'

import React, { useState } from 'react'
import cn from 'classnames'

function NewWorkItem(props) {
    const [favor, setFavor] = useState(false)
    const [cart, setCart] = useState(false)
    return (
        <div className="new-works-section__item new-work js-grid-item" data-href="#">
            <img className="new-work__img swiper-lazy" src={props.data.imgPath} alt="Распоряжения о структуре тут длинное название картины" />
            <a className="new-work__title" href="#">{props.data.title}
                <div className="new-work__title-label">
                    {props.data.novelty && <span className="new-work__novelty">Новинка</span>}
                    {!!+props.data.discount && <span className="new-work__discount" data-discount={`${props.data.discount}%`}>Скидка&nbsp;</span>}
                </div>
            </a>
            <a className="new-work__auth" href="#">{props.data.author}</a>
            <div className="new-work__descr">{props.data.desc}</div>
            <div className="new-work__footer">
                <div className="new-work__footer-price-wrapper">
                    <div className="new-work__footer-price">{props.data.newPrice} <span className="ruble">&nbsp;7</span></div>
                    <div className="new-work__footer-old-price">{props.data.oldPrice}</div>
                </div>
                <div className="new-work__footer-icon-wrapper">
                    <svg className={cn("new-work__footer-icon", { "active": favor })} onClick={() => { setFavor(!favor) }}>
                        <use xlinkHref="#favor"></use>
                    </svg>
                    <svg className={cn("new-work__footer-icon", { "active": cart })} onClick={() => { setCart(!cart) }}>
                        <use xlinkHref="#cart"> </use>
                    </svg>
                </div>
            </div>
        </div>
    )
}
export default NewWorkItem
