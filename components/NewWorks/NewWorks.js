import './NewWorks.scss'

import React, { Component } from 'react'
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry"
import cn from 'classnames'

import TitleStd from '../TitleStd/TitleStd'
import NewWorkItem from '../NewWorkItem/NewWorkItem'
import NewWorkChoice from '../NewWorkChoice/NewWorkChoice'
import ButtonStd from '../ButtonStd/ButtonStd';

class NewWorks extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    choiceBlockIndexChange = () => {
        if (window.matchMedia("(min-width: 0px) and (max-width: 767px)").matches) {
            this.setState({
                choiceBlock: false,
                choiceBlockIndex: 3,
                gutter: 5,
            })
        }
        else if (window.matchMedia("(min-width: 768px) and (max-width: 1279px)").matches) {
            this.setState({
                choiceBlock: true,
                choiceBlockIndex: 2,
                gutter: 10,
            })
        }
        else if (window.matchMedia("(min-width: 1280px)").matches) {
            this.setState({
                choiceBlock: true,
                choiceBlockIndex: 3,
                gutter: 40,
            })
        }
    }
    componentDidMount() {
        this.choiceBlockIndexChange();
        window.addEventListener('resize', this.choiceBlockIndexChange);

    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.choiceBlockIndexChange);
    }
    render() {
        let newItems = this.props.data.items
        let items = newItems.map((item, ind, arr) => <NewWorkItem data={arr[ind]} key={ind} />)
        if (this.state.choiceBlock && this.props.hasChoiceBlock) {
            items.splice(this.state.choiceBlockIndex, 0, <NewWorkChoice key={'wer234werwr'} />)
        }
        return (
            <section className={`new-works-section ${this.props.modClass}`}>
                {this.props.hasTitle && <TitleStd text='Новые работы' modClass='new-works-section__title' />}
                <div className="container relative">
                    <div className={cn("sort", {
                        "new-works-section__sort-no-hide": !this.props.hideSortBlockOnMobile,
                        "new-works-section__sort-hide": this.props.hideSortBlockOnMobile
                    })}>
                        <label className="sort__title" htmlFor="sort">Сортировать по:</label>
                        <select className="sort__categories" style={{ width: '300px' }} name="sort">
                            <option className="sort__option">Дате </option>
                            <option className="sort__option">Автору</option>
                            <option className="sort__option">Стилю </option>
                            <option className="sort__option">Названию </option>
                            <option className="sort__option">Типу</option>
                        </select>
                    </div>
                    <div className="new-works-section__work">
                        <ResponsiveMasonry columnsCountBreakPoints={{ 0: 2, 768: 3, 1280: 4 }} >
                            <Masonry gutter={this.state.gutter} columnsCount={4}>
                                {items}
                            </Masonry>
                        </ResponsiveMasonry>
                    </div >
                </div >
                {this.props.showAllButton && <ButtonStd modClass='new-works-section__btn' text='Смотреть все' />}
                {this.props.showMoreButton && <ButtonStd modClass='new-works-section__btn' text='Показать еще' />}
            </section >
        )
    }
}
export default NewWorks
