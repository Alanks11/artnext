import './Review.scss'

import React from 'react'
import SwiperCore, { Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

import TitleStd from '../TitleStd/TitleStd'

SwiperCore.use([Navigation]);

const reviewSliderConfig = {
    loop: true,
    slidesPerView: 1,
    navigation: {
        nextEl: '.review .js-next',
        prevEl: '.review .js-prev',
    },
    breakpoints: {
        767: {
            slidesPerView: 2,
        }
    }
}

function Review({ data }) {
    const reviewSlides = data.slides
    return (
        <section className="review">
            < TitleStd text='Обзор стилей' modClass='review__title-std' />
            <div className="container relative">
                <div className="review__slider-controls">
                    <div className="review__prev js-prev slider-control-btn--prev slider-control-btn "></div>
                    <div className="review__next js-next undefined slider-control-btn "></div>
                </div>
                <Swiper {...reviewSliderConfig} className="review__slider-container swiper-container">
                    <div className="review__slider-wrapper swiper-wrapper">
                        {reviewSlides.map(({ part1, part2, part3 }, ind) => {
                            return < SwiperSlide key={ind} className="review__slide swiper-slide review-item " >
                                {
                                    ind % 2 !== 0 &&
                                    <div className="review-item__row">
                                        <div className="review-item__card review-item__card--wide">
                                            <a className="review-item__img review-item__img--wide" style={{ backgroundImage: `url('${part1.imgPath}')` }} href="#" ></a>
                                            <a className="review-item__text-top review-item__text-top--wide" href="#">{part1.title}</a>
                                            <a className="review-item__text-bottom" href="#">{part1.subtitle}</a>
                                        </div>
                                    </div>
                                }
                                <div className="review-item__row" >
                                    <div className="review-item__card">
                                        <a className="review-item__img" style={{ backgroundImage: `url('${part2.imgPath}')` }} href="#" ></a>
                                        <a className="review-item__text-top" href="#">{part2.title}</a>
                                        <a className="review-item__text-bottom" href="#">{part2.subtitle}</a>
                                    </div>
                                    <div className="review-item__card"><a className="review-item__img" style={{ backgroundImage: `url('${part3.imgPath}')` }} href="#" ></a>
                                        <a className="review-item__text-top" href="#">{part3.title}</a>
                                        <a className="review-item__text-bottom" href="#">{part2.subtitle}</a>
                                    </div>
                                </div >
                                {
                                    ind % 2 === 0 &&
                                    <div className="review-item__row">
                                        <div className="review-item__card review-item__card--wide">
                                            <a className="review-item__img review-item__img--wide" style={{ backgroundImage: `url('${part1.imgPath}')` }} href="#" ></a>
                                            <a className="review-item__text-top review-item__text-top--wide" href="#">{part1.title}</a>
                                            <a className="review-item__text-bottom" href="#">{part1.subtitle}</a>
                                        </div>
                                    </div>
                                }
                            </SwiperSlide >
                        })}
                    </div >
                </Swiper >
            </div >
        </section >
    )
}
export default Review
