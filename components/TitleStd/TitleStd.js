import './TitleStd.scss'

function TitleStd(props) {
    return (
        <div className={`title-std ${props.modClass}`}>
            <h2 className="title-std__content">{props.text}</h2>
        </div>
    )
}
export default TitleStd
