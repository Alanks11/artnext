const fs = require('fs');
const path = require('path');

let name = process.argv[2];
let cls = process.argv.indexOf('cls') >= 0;
let m = process.argv.indexOf('m') >= 0;

let typeOfComponent = cls ? 'cls' : 'fnc';
let typeOfCSS = m ? 'module' : 'vanilla';


let options = {
    path: path.resolve(__dirname, 'components'),
    js: {
        cls: {
            content: `
class ${name} extends Component {
    constructor(props){
        super(props);
        this.state = {};
    }
    render(){
        return(
            <div className="${name}"></div>
        )
    }
}

export default ${name}
            `,
            ext: 'js',
            importReact: `import React, {Component} from 'react';`
        },
        fnc: {
            content: `
function ${name}(props){
    return(
        <div className="${name}"></div>
    )
}

export default ${name}
            `,
            ext: 'js',
            importReact: `import React from 'react';`
        }
    },
    css: {
        vanilla: {
            fileName: `${name}.scss`,
            import: `import './${name}.scss'`,
            content: `
@import '../../styles/vars';
@import '../../styles/mixins';
            `
        },
        module: {
            fileName: `${name}.module.scss`,
            import: `import styles from './${name}.module.scss'`,
            content: `.${name} {}`
        }
    }

}
//create folder
fs.mkdirSync(`${options.path}/${name}`);
//create js
fs.writeFileSync(`${options.path}/${name}/${name}.${options.js[typeOfComponent]['ext']}`,
    `${options.css[typeOfCSS]['import']}
    ${options.js[typeOfComponent]['importReact']}
    ${options.js[typeOfComponent]['content']}`)
//create css
fs.writeFileSync(`${options.path}/${name}/${options.css[typeOfCSS]['fileName']}`,
    `${options.css[typeOfCSS]['content']}`);

console.log(`import ${name} from './components/${name}/${name}';`);

