import React, { useState, useEffect } from 'react'

export function useCheckInput(checkCondition) {
    const [inputError, setInputError] = useState(false)
    const [inputValue, setInputValue] = useState('')
    const [firstValidate, setFirstValidate] = useState(true)
    return {
        validate: () => {
            setFirstValidate(false)
            if (checkCondition(inputValue)) {
                setInputError(false)
                return true
            } else {
                setInputError(true)
                return false
            }
        },
        setValue: (e) => {
            setInputValue(e.currentTarget.value)
            if (!firstValidate) {
                if (checkCondition(e.currentTarget.value)) {
                    setInputError(false)
                } else {
                    setInputError(true)
                }
            }
        },
        inputError,
        inputValue,
    }
}

export function useClickOut(clickInAction = null, clickOutAction = null) {
    const refTarget = React.createRef();
    const refToggler = React.createRef();
    function clickOutHandler(e) {
        if (refToggler.current) {
            if (e.target === refToggler.current || refToggler.current.contains(e.target)) {
                return;
            } else {
                if (refTarget.current) {
                    if (e.target === refTarget.current || refTarget.current.contains(e.target)) {
                        if (typeof clickInAction === 'function') {
                            clickInAction();
                        }
                    } else {
                        if (typeof clickOutAction === 'function') {
                            clickOutAction();
                        }
                    }
                }
            }
        }
    }
    useEffect(() => {
        document.addEventListener('click', clickOutHandler);
        return () => {
            document.removeEventListener('click', clickOutHandler);
        }
    }, [refTarget])
    return [refTarget, refToggler]
}