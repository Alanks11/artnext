
import '../styles/reset.scss'
import '../styles/vars.scss'
import '../styles/fonts.scss'
import '../styles/mixins.scss'
import '../styles/common.scss'

import 'swiper/swiper.min.css';
import 'swiper/components/navigation/navigation.min.css';
import 'swiper/components/pagination/pagination.min.css';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
