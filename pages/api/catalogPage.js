export default (req, res) => {
    res.statusCode = 200
    res.json(catalogData)
}

const catalogData = {
    filters: {
        authorOptions: [
            {
                label: 'А',
                options: [
                    { label: "Абулафия Крайнова", value: "Абулафия Крайнова" },
                    { label: "Арменинов Тримедул", value: "Арменинов Тримедул" },
                    { label: "Асадуллин Нефедов", value: "Асадуллин Нефедов" },
                    { label: "Ашихмин Шадурахов", value: "Ашихмин Шадурахов" }
                ]
            },
            {
                label: 'Б',
                options: [
                    { label: "Бабурин Константин", value: "Бабурин Константин" },
                    { label: "Бабуркин Вениамин", value: "Бабуркин Вениамин" },
                    { label: "Баламутин Перлпмутр", value: "Баламутин Перлпмутр" },
                    { label: "Бенвенист Трамп", value: "Бенвенист Трамп" },
                    { label: "Бердюгин Битюг", value: "Бердюгин Битюг" }
                ]
            }
        ],
        jenreOptions: [
            {
                label: 'А',
                options: [
                    { label: "Абстрактная живопись", value: "Абстрактная живопись" },
                    { label: "Аутентичные картины", value: "Аутентичные картины" },
                ]
            },
            {
                label: 'Б',
                options: [
                    { label: "Бытовой", value: "Бытовой" },
                    { label: "Батальный", value: "Батальный" },
                ]
            }
        ],
        colorOptions: [
            {
                label: 'А',
                options: [
                    { label: "Аквамарин", value: "Аквамарин" },
                    { label: "Антикварный", value: "Антикварный" },
                ]
            },
            {
                label: 'Б',
                options: [
                    { label: "Бирюзовый", value: "Бирюзовый" },
                    { label: "Бриллиантовый", value: "Бриллиантовый" },
                ]
            }
        ]

    },
    slides: [
        {
            text: 'Барокко',
            imgPath: 'img/catalog_slider1.jpg'
        },
        {
            text: 'Живопись',
            imgPath: 'img/catalog_slider2.jpg'
        },
        {
            text: 'Пейзаж',
            imgPath: 'img/catalog_slider3.jpg'
        },
        {
            text: 'Поп-арт',
            imgPath: 'img/catalog_slider4.jpg'
        },
        {
            text: 'Модерн',
            imgPath: 'img/catalog_slider5.jpg'
        },
        {
            text: 'Импрессионизм',
            imgPath: 'img/catalog_slider3.jpg'
        },
    ],
    sameWorks: {
        items: [
            {
                imgPath: 'img/work1.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 20,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work2.jpg',
                title: 'Абстракция, Опус J202',
                novelty: false,
                discount: 3,
                author: 'Соколов Виталий',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work3.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 0,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work4.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: false,
                discount: 0,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work5.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 20,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work6.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 20,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work7.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 20,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work8.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 20,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,

            },
            {
                imgPath: 'img/work9.jpg',
                title: 'Распоряжения о структуре тут длинное название картины',
                novelty: true,
                discount: 20,
                author: 'Екатерина Александровна Преображенская',
                desc: 'Картина, 80x60 см.',
                newPrice: 987000,
                oldPrice: 100399,
            },
        ]
    }
}
