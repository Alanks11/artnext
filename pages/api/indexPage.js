export default (req, res) => {
  res.statusCode = 200
  res.json(indexPageData)
}

const indexPageData = {
  hSlider: {
    slides: [
      {
        imagePath: 'img/header_slide1.jpg',
        pretitle: 'Новое на этой неделе',
        title: 'Лучшие фигуративные произведения',
        desc: 'Новые оригинальные работы, выбранные нашим экспертом'
      },
      {
        imagePath: 'img/header_slide2.jpg',
        pretitle: 'Неновое не на этой неделе',
        title: 'Нелучшие фигуративные произведения',
        desc: 'Неновые неоригинальные работы, выбранные нашим экспертом'
      },
      {
        imagePath: 'img/header_slide3.jpg',
        pretitle: 'Сверхновое на этой неделе',
        title: 'Сверхлучшие фигуративные произведения',
        desc: 'Сверхновые сверхоригинальные работы, выбранные нашим экспертом'
      },

    ]
  },
  newWorks: {
    items: [
      {
        imgPath: 'img/work1.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 20,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work2.jpg',
        title: 'Абстракция, Опус J202',
        novelty: false,
        discount: 3,
        author: 'Соколов Виталий',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work3.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 0,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work4.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: false,
        discount: 0,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work5.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 20,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work6.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 20,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work7.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 20,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work8.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 20,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,

      },
      {
        imgPath: 'img/work9.jpg',
        title: 'Распоряжения о структуре тут длинное название картины',
        novelty: true,
        discount: 20,
        author: 'Екатерина Александровна Преображенская',
        desc: 'Картина, 80x60 см.',
        newPrice: 987000,
        oldPrice: 100399,
      },
    ]
  },
  review: {
    slides: [
      {
        part1: {
          imgPath: 'img/review1.jpg',
          title: 'Современное НЮ',
          subtitle: 'Новое изобразительное искусство',
        },
        part2: {
          imgPath: 'img/review2.jpg',
          title: 'Вдохновение Уорхоллом',
          subtitle: 'Новый поп-арт',
        },
        part3: {
          imgPath: 'img/review3.jpg',
          title: 'Вдохновение Хансом Хофманном',
          subtitle: 'Новый абстрактный экспрессионизм',
        },
      },
      {
        part1: {
          imgPath: 'img/review4.jpg',
          title: 'Современный пейзаж',
          subtitle: 'Березы как основной лейтмотив русской живописи',
        },
        part2: {
          imgPath: 'img/review5.jpg',
          title: 'Кубизм как математическое явление',
          subtitle: 'Рисуем n-мерный куб с помощью масла и фантазии',
        },
        part3: {
          imgPath: 'img/review6.jpg',
          title: 'Портретизм',
          subtitle: 'Количество пикселей - не залог четкого портрета!',
        },
        part1: {
          imgPath: 'img/review1.jpg',
          title: 'Современное НЮ',
          subtitle: 'Новое изобразительное искусство',
        },
        part2: {
          imgPath: 'img/review2.jpg',
          title: 'Вдохновение Уорхоллом',
          subtitle: 'Новый поп-арт',
        },
        part3: {
          imgPath: 'img/review3.jpg',
          title: 'Вдохновение Хансом Хофманном',
          subtitle: 'Новый абстрактный экспрессионизм',
        },
      },
      {
        part1: {
          imgPath: 'img/review4.jpg',
          title: 'Современный пейзаж',
          subtitle: 'Березы как основной лейтмотив русской живописи',
        },
        part2: {
          imgPath: 'img/review5.jpg',
          title: 'Кубизм как математическое явление',
          subtitle: 'Рисуем n-мерный куб с помощью масла и фантазии',
        },
        part3: {
          imgPath: 'img/review6.jpg',
          title: 'Портретизм',
          subtitle: 'Количество пикселей - не залог четкого портрета!',
        },
      },
    ]
  },
  artNews: {
    slides: [
      {
        imgPath: 'img/new1.jpg',
        category: 'Галереи и выставки',
        date: '21.12.2020',
        title: 'Обзор нашего эксперта Владимира Рубинштейна-Загорского',
        desc: 'Теперь, чтобы увидеть и сравнить все 36 картин культового голландца, не нужно объезжать весь мир.'
      },
      {
        imgPath: 'img/new2.jpg',
        category: 'Коллеционирование',
        date: '18.12.2020',
        title: 'Обзор нашего эксперта Иосифа Сталина',
        desc: 'Чтобы расстрэлять художника, не обязательно нужно смотрэть на его картыну.'
      },
      {
        imgPath: 'img/new1.jpg',
        category: 'Галереи и выставки',
        date: '21.12.2020',
        title: 'Обзор нашего эксперта Владимира Рубинштейна-Загорского',
        desc: 'Теперь, чтобы увидеть и сравнить все 36 картин культового голландца, не нужно объезжать весь мир.'
      },
      {
        imgPath: 'img/new2.jpg',
        category: 'Коллеционирование',
        date: '18.12.2020',
        title: 'Обзор нашего эксперта Иосифа Сталина',
        desc: 'Чтобы расстрэлять художника, не обязательно нужно смотрэть на его картыну.'
      },
    ]
  }

}