export default (req, res) => {
    res.statusCode = 200
    res.json(itemPageData)
}

const itemPageData = {
    sameWorks: [
        {
            imgPath: 'img/work1.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 20,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work2.jpg',
            title: 'Абстракция, Опус J202',
            novelty: false,
            discount: 30,
            author: 'Соколов Виталий',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work3.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 0,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work4.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: false,
            discount: 0,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work5.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 20,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work6.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 20,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work7.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 20,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work8.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 20,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,

        },
        {
            imgPath: 'img/work9.jpg',
            title: 'Распоряжения о структуре тут длинное название картины',
            novelty: true,
            discount: 20,
            author: 'Екатерина Александровна Преображенская',
            desc: 'Картина, 80x60 см.',
            newPrice: 987000,
            oldPrice: 100399,
        },
    ],
    card: {
        previews: [
            'img/item1.jpg',
            'img/item2.jpg',
            'img/item3.jpg',
            'img/item4.jpg',
            'img/item5.jpg',
            'img/item6.jpg'
        ],
        images: [
            'img/item1.jpg',
            'img/item2.jpg',
            'img/item3.jpg',
            'img/item4.jpg',
            'img/item5.jpg',
            'img/item6.jpg'
        ],
        description: {
            title: 'Распоряжения о структуре очень длинное название картины',
            articul: 'Арт. HWV-165123',
            author: 'Диана Миллирелло',
            size: ' 100 x 120 x 4 см',
            style: 'Современный пейзаж',
            price: '927 750',
            oldPrice: '727 750',
            year: 2008,
            theme: 'Океан, берег, пляж',
            materials: 'Уголь, чернила, пастель',
            aboutItem: 'Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия'
        }
    }
}