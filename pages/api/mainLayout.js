// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
  res.statusCode = 200
  res.json(mainLayout)
}

const mainLayout = {
  header: {
    langs: ['Ru', 'Eng', 'Spanish', 'Clingon', 'Martian'],
    currentLang: 'Ru',
    authUser: false,
    userID: null,
    favor: 0,
    cart: 0,
  },
}