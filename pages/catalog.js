import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import SvgSprite from '../components/SvgSprite/SvgSprite';
import CatalogTitle from '../components/CatalogTitle/CatalogTitle';
import CatalogFilters from '../components/CatalogFilters/CatalogFilters';
import CatalogSlider from '../components/CatalogSlider/CatalogSlider';
import NewWorks from '../components/NewWorks/NewWorks'
import MainLayout from '../components/MainLayout/MainLayout';


function Catalog({ data }) {
    return (
        <MainLayout headerInverted={true}>
            <CatalogTitle />
            <CatalogFilters {...data.filters} />
            <CatalogSlider slides={data.slides} />
            <NewWorks modClass='new-works-section--without-title' data={data.sameWorks} hasTitle={false} hideSortBlockOnMobile={false} hasChoiceBlock={false} showMoreButton={false} showAllButton={true} />
        </MainLayout>
    )
}

export async function getServerSideProps() {
    const res = await fetch(`http://localhost:3000/api/catalogPage`)
    const data = await res.json()
    return { props: { data } }
}

export default Catalog