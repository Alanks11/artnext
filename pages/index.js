import Hslider from '../components/Hslider/Hslider';
import Review from '../components/Review/Review';
import ArtNews from '../components/ArtNews/ArtNews';
import Beginning from '../components/Beginning/Beginning';
import NewWorks from '../components/NewWorks/NewWorks';
import MainLayout from '../components/MainLayout/MainLayout';

function Main({ data }) {
  return (
    <>
      <MainLayout headerInverted={false}>
        <Hslider data={data.hSlider} />
        <Beginning />
        <NewWorks data={data.newWorks} modClass='new-works-section--without-title' hasTitle={true} hideSortBlockOnMobile={true} hasChoiceBlock={true} showMoreButton={true} showAllButton={false} />
        <Review data={data.review} />
        <ArtNews data={data.artNews} />
      </MainLayout>
    </>
  )
}

export async function getServerSideProps() {
  const res = await fetch(`http://localhost:3000/api/indexPage`)
  const data = await res.json()
  return { props: { data } }
}

// export async function getServerSideProps() {
//   const res1 = await fetch(`http://localhost:3000/api/indexPage`)
//   const res2 = await fetch(`http://localhost:3000/api/itemPage`)
//   let [data1, data2] = await Promise.all([res1.json(), res2.json()])
//   const data = { ...data1, ...data2 }
//   console.log(data);
//   return { props: { data } }
// }

export default Main