import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import SvgSprite from '../components/SvgSprite/SvgSprite'
import Breadcrumbs from '../components/Breadcrumbs/Breadcrumbs'
import ItemCard from '../components/ItemCard/ItemCard'
import ItemWorks from '../components/ItemWorks/ItemWorks'
import MainLayout from '../components/MainLayout/MainLayout'

export default function Item({ data }) {
    return (
        <MainLayout headerInverted={true}>
            <Breadcrumbs />
            <ItemCard {...data.card} />
            <ItemWorks sameWorks={data.sameWorks} />
        </MainLayout>
    )
}

export async function getServerSideProps() {
    const res = await fetch(`http://localhost:3000/api/itemPage`)
    const data = await res.json()
    return { props: { data } }
}
